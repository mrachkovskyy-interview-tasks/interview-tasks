package com.mrachkovskyy.interview;

import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ReferenceImpl extends Java8SDK {

    public ReferenceImpl(UserDao userDao) {
        super(userDao);
    }

    @Override
    public String getMiddleNameOrCurrentTimestamp(String name) {
        return userDao.findUserByName(name)
                      .flatMap(User::getMiddleName)
                      .orElseGet(() -> String.valueOf(System.currentTimeMillis()));
    }

    @Override
    public List<String> getHobbies() {
        return userDao.findAll()
                      .stream()
                      .filter(user -> user.getName().startsWith("A"))
                      .filter(user -> user.getMiddleName().isPresent())
                      .flatMap(user -> user.getHobbies().stream())
                      .distinct()
                      .collect(Collectors.toList());
    }

    @Override
    public Stream<User> streamUser() {
        return StreamSupport.stream(new UserSpliterator(userDao, 3), false);
    }

    public static class UserSpliterator implements Spliterator<User> {

        private final UserDao userDao;
        private final int pageSize;

        public UserSpliterator(UserDao userDao, int pageSize) {
            this.userDao = userDao;
            this.pageSize = pageSize;
        }

        private List<User> page;
        private int index = 0;
        private int offset = 0;

        @Override
        public boolean tryAdvance(Consumer<? super User> action) {
            if (page == null || index == pageSize) {
                page = userDao.findAll(offset, pageSize);
                offset = offset + pageSize;
                index = 0;
            }
            if (index == page.size()) {
                return false;
            }
            action.accept(page.get(index++));
            return true;
        }

        @Override
        public Spliterator<User> trySplit() {
            return null;
        }

        @Override
        public long estimateSize() {
            return Long.MAX_VALUE;
        }

        @Override
        public int characteristics() {
            return NONNULL;
        }
    }
}
