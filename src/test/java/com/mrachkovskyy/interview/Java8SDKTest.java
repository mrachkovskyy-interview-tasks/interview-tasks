package com.mrachkovskyy.interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Java8SDKTest {

    @Test
    void streamUser() {
        TestUserDao userDao = new TestUserDao();
        ReferenceImpl test = new ReferenceImpl(userDao);
        List<Java8SDK.User> actual = test.streamUser()
                                         .collect(Collectors.toList());
        Assertions.assertEquals(userDao.findAll(), actual);
    }

    private static class TestUserDao implements Java8SDK.UserDao {

        private final List<Java8SDK.User> users = new ArrayList<>();

        public TestUserDao() {
            users.add(new Java8SDK.User("Dima", null, Arrays.asList("H1", "H2")));
            users.add(new Java8SDK.User("Peter ", "Olegovich", Arrays.asList("H1", "H3")));
            users.add(new Java8SDK.User("Olga", "null", Arrays.asList("H7", "H5", "H8")));
            users.add(new Java8SDK.User("Slava", "Sergeevich", Arrays.asList("H4", "H6")));
        }

        @Override
        public Optional<Java8SDK.User> findUserByName(String name) {
            return users.stream()
                        .filter(user -> user.getName().equals(name))
                        .findFirst();
        }

        @Override
        public List<Java8SDK.User> findAll() {
            return new ArrayList<>(users);
        }

        @Override
        public List<Java8SDK.User> findAll(int offset, int count) {
            if (offset < users.size()) {
                int toIndex = offset + count;
                if (toIndex < users.size()) {
                    System.out.println("Retrieve users from " + offset + " to " + toIndex);
                    return new ArrayList<>(users.subList(offset, toIndex));
                } else {
                    System.out.println("Retrieve users from " + offset + " to " + users.size());
                    return new ArrayList<>(users.subList(offset, users.size()));
                }
            } else {
                System.out.println("No more users");
                return Collections.emptyList();
            }
        }
    }
}
